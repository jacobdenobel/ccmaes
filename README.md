# ConfigurableCMAES

Running tests
`$ python3 -m unittest discover`

Running optimizer
`$ python3 -m ccmaes  [-h] [-f FID] [-d DIM] [-i ITERATIONS] [-l] [-c] [-L LABEL]
                   [-s SEED] [-a ARGUMENTS [ARGUMENTS ...]]`


[Documentation](https://ccmaes.readthedocs.io/)