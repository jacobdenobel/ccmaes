halton_sampling
===============

.. currentmodule:: ccmaes.sampling

.. autofunction:: halton_sampling
