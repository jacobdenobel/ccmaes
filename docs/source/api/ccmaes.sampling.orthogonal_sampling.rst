orthogonal_sampling
===================

.. currentmodule:: ccmaes.sampling

.. autofunction:: orthogonal_sampling
