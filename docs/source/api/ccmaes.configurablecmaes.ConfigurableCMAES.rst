ConfigurableCMAES
=================

.. currentmodule:: ccmaes.configurablecmaes

.. autoclass:: ConfigurableCMAES
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~ConfigurableCMAES.break_conditions

   .. rubric:: Methods Summary

   .. autosummary::

      ~ConfigurableCMAES.fitness_func
      ~ConfigurableCMAES.mutate
      ~ConfigurableCMAES.recombine
      ~ConfigurableCMAES.run
      ~ConfigurableCMAES.select
      ~ConfigurableCMAES.sequential_break_conditions
      ~ConfigurableCMAES.step

   .. rubric:: Attributes Documentation

   .. autoattribute:: break_conditions

   .. rubric:: Methods Documentation

   .. automethod:: fitness_func
   .. automethod:: mutate
   .. automethod:: recombine
   .. automethod:: run
   .. automethod:: select
   .. automethod:: sequential_break_conditions
   .. automethod:: step
