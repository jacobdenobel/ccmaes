AnnotatedStructMeta
===================

.. currentmodule:: ccmaes.utils

.. autoclass:: AnnotatedStructMeta
   :show-inheritance:
