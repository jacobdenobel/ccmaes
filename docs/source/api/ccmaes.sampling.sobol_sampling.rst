sobol_sampling
==============

.. currentmodule:: ccmaes.sampling

.. autofunction:: sobol_sampling
