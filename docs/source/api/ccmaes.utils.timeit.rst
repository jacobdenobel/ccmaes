timeit
======

.. currentmodule:: ccmaes.utils

.. autofunction:: timeit
