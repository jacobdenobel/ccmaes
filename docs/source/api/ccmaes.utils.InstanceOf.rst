InstanceOf
==========

.. currentmodule:: ccmaes.utils

.. autoclass:: InstanceOf
   :show-inheritance:
