Descriptor
==========

.. currentmodule:: ccmaes.utils

.. autoclass:: Descriptor
   :show-inheritance:
