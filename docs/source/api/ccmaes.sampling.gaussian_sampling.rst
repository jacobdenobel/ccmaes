gaussian_sampling
=================

.. currentmodule:: ccmaes.sampling

.. autofunction:: gaussian_sampling
