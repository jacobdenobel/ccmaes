Welcome to CCMAES's Documentation!
==================================

.. automodapi:: ccmaes.configurablecmaes
    :members:

.. automodapi:: ccmaes.parameters
    :members:

.. automodapi:: ccmaes.population
    :members:

.. automodapi:: ccmaes.sampling
    :members:

.. automodapi:: ccmaes.utils
    :members:

.. automodapi:: ccmaes.asktellcmaes
    :members:



.. toctree::
   :maxdepth: 2
   :caption: Contents:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
